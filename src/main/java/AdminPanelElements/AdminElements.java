package AdminPanelElements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AdminElements {

    //Login Page Elements

    @FindBy(how = How.XPATH, using = "//div[@class='mainlogo navbar-brand']/img/@src")
    protected
    WebElement LoginpageLogo;

    @FindBy(how = How.ID, using = "UserName")
    protected
    WebElement Username;

    @FindBy (how = How.ID, using = "Password")
    protected
    WebElement Password;

    @FindBy (how = How.XPATH, using = "//*[@action]/div/fieldset/p/input")
    protected
    WebElement LoginButton;

    // Admin Dashboard Elements


    @FindBy (how = How.ID, using = "showCreatePath")
    protected
    WebElement CreatePathButton;



    @FindBy(how = How.ID, using = "term")
    protected
    WebElement PathCreatedByIT;

    @FindBy(how =  How.XPATH, using = "//*[@id='Grid']/table/thead/tr/th[3]/a")
    protected
    WebElement sortpath;



    @FindBy(how = How.XPATH,using = "//div[@class='col-sm-12 uc-typ-container']")
    protected
    WebElement PathsLocation;

    //---------------//

    //Create Path Window

    @FindBy(how = How.ID, using = "PathName")
    protected
    WebElement PathNamefield;

    @FindBy(how = How.ID, using = "Flow")
    protected
    WebElement Templatedropdown;
    @FindBy(how = How.XPATH, using = "//*[@id='create-path-new-template']/option[2]")
    protected
    WebElement FreestyleTemplate;


    @FindBy(how = How.ID, using = "SitePathLabel")
    protected
    WebElement PathLabelField;

    @FindBy(how = How.XPATH, using = "(//input[@id='btn-createpath'])[2]")
    protected
    WebElement SavePathButton;

    @FindBy(how = How.XPATH, using = "//*[@id='create-path-new-template']/option[3]")
    protected
    WebElement NewTemplate;

    //-----------//

    // Delete Path


    @FindBy(how = How.XPATH, using = "(//i[@class='iconSetSubNav icon-trash'])[1]")
    protected
    WebElement DeleteIcon;

    @FindBy(how = How.ID, using = "okID")
    protected
    WebElement DeleteConfirm;


    //------------------//

    // Log Out

    @FindBy(how = How.XPATH, using = "//a[@class='logout-link']")
    protected
    WebElement LogoutButton;

    //FB Elements

    @FindBy(how = How.ID, using = "email")
    protected
    WebElement FBEmail;
    @FindBy(how = How.ID, using = "pass")
    protected
    WebElement FBPass;
    @FindBy(how = How.ID, using = "loginbutton")
    protected
    WebElement FBLog;




}
