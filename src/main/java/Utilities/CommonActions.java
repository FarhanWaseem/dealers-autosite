package Utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;
import java.util.Random;

public class CommonActions {

    WebDriverWait wait;
    WebDriver driver;

    public CommonActions(WebDriver driver, WebDriverWait wait) {
        this.wait = wait;
        this.driver = driver;
    }

    public void navigateToUrl(String url) {
        driver.get(url);
        Helper.waitForPageLoad(driver);
        System.out.println("Go to: " + url);
    }

    public static void clickElementJs(WebDriver driver, WebElement element) {
        try {
            JavascriptExecutor e = (JavascriptExecutor) driver;
            Helper.highlight(driver, element);
            e.executeScript("arguments[0].click();", element);
        } catch (Exception var3) {
            Assert.fail("can not click element invisible " + var3.getMessage());
        }
    }

    public void focusElementJs(WebDriver driver, WebElement element) {
        try {
            JavascriptExecutor e = (JavascriptExecutor) driver;
            Helper.highlight(driver, element);
            e.executeScript("arguments[0].focus();", element);
        } catch (Exception e) {

        }
    }

    public void sendKeysToWebElement(WebElement element, String text) throws InterruptedException {
        WebElement elementVisible = Waits.waitVisibilityOfElement(wait, element);
        try {
            focusElementJs(driver, elementVisible);
            elementVisible.sendKeys(text);
        } catch (Exception e) {

        }
    }

    /**
     * Check if the element that receive as a parameter
     *  @param elementToVerify
     * @param errorMessage
     * @param AuxMethods
     */
    public void checkVisibilityOfELement(WebElement elementToVerify, String errorMessage, Helper AuxMethods) throws InterruptedException {
        WebElement elementVisible = Waits.waitVisibilityOfElement(wait, elementToVerify);
        if (elementVisible == null) {
            Assert.fail(errorMessage);
        } else {
            highLightElement(elementVisible, AuxMethods);
        }
    }

    public boolean checkVisibilityOfElement(WebElement elementToVerify) throws InterruptedException {
        WebElement elementVisible = Waits.waitVisibilityOfElement(wait, elementToVerify);
        return elementVisible != null;
    }

    public boolean checkVisibilityOfElements(List<WebElement> elementsToVerify) throws InterruptedException {
        return Waits.waitVisibilityOfTwoElements(wait, elementsToVerify.get(0), elementsToVerify.get(1));
    }

    /**
     * Highligh a element when is present on the page.
     *
     * @param elementToVerify
     * @param AuxMethods
     * @throws InterruptedException
     */
    public void highLightElement(WebElement elementToVerify, Helper AuxMethods) throws InterruptedException {
        AuxMethods.highlight(driver, elementToVerify);
        Thread.sleep(500);
    }

    /**
     * Select an specific option from drop-down list that is received as a parameter.
     *
     * @param select
     */

    private void selectSpecificOption(WebElement select, String text) {
        try {
            WebElement currentSelect = Waits.waitVisibilityOfElement(wait, select);
            Select dropdown = new Select(select);
            String valueOfOption = "";
            currentSelect.click();
            List<WebElement> options = currentSelect.findElements(By.xpath("option"));
            for (int i = 0; i < options.size(); ++i) {
                valueOfOption = (options.get(i)).getText();
                if (valueOfOption.trim().equalsIgnoreCase(text)) {
                    WebElement elementToSelect = options.get(i);
                    elementToSelect.click();
                    dropdown.selectByVisibleText(text);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String selectDropdownOption(WebElement select, String text) {
        try {
            Select dropdown = new Select(select);
            dropdown.selectByVisibleText(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }

    /**
     * Selects an specific option from drop-down list that contains the value sent as a parameter.
     * @param select WebElement The select web element.
     * @param containedText String The value to search on select options.
     */
    public void selectOptionThatContains(WebElement select, String containedText) {
        try {
            WebElement currentSelect = Waits.waitVisibilityOfElement(wait, select);
            Select dropdown = new Select(select);
            String valueOfOption = "";
            currentSelect.click();
            List<WebElement> options = currentSelect.findElements(By.xpath("option"));
            for (int i = 0; i < options.size(); ++i) {
                valueOfOption = (options.get(i)).getText();
                if (valueOfOption.trim().toLowerCase().contains(containedText.toLowerCase()))
                    dropdown.selectByVisibleText(valueOfOption);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Select a random option of the select that receive as a parameter.
     *
     * @param select
     */
    private  String selectRandomOptionDropDown(WebElement select) {
        String nameOfOption = "";
        try {
            Random rand = new Random();
            WebElement currentSelect = Waits.waitVisibilityOfElement(wait, select);
            Select dropdown = new Select(select);
            currentSelect.click();
            List<WebElement> options = currentSelect.findElements(By.xpath("option"));
            int optionNumber;
            if (options.size() > 0) {
                optionNumber = rand.nextInt(options.size());
                if(options.size() > 1)
                    optionNumber = optionNumber == 0 ? 1 : optionNumber;
            } else {
                optionNumber = 1;
            }
            Waits.waitForElementToBeClickable(wait, options.get(optionNumber));
            WebElement elementToSelect = options.get(optionNumber);
            nameOfOption = elementToSelect.getText();
            elementToSelect.click();
            dropdown.selectByVisibleText(nameOfOption);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Fail: " + e.getMessage());
        }
        return nameOfOption;
    }

    /**
     * Select an option for the make, model or trim selects.
     * @param element
     * @param value
     * @return
     */
    public String selectOption(WebElement element,  String value){
        if(!value.equalsIgnoreCase("random")){
            selectSpecificOption(element,value);
        }else{
            value = selectRandomOptionDropDown(element);
        }
        return value;
    }

//    /**
//     * \]
//     *
//     * @param elementToVerify
//     * @param value
//     * @param AuxMethods
//     * @throws InterruptedException
//     */
//    public void fillInput(WebElement elementToVerify, String value, Helper AuxMethods) throws InterruptedException {
//        WebElement elementVisible = Waits.waitVisibilityOfElement(wait, elementToVerify);
//        if (elementVisible != null) {
//            highLightElement(elementVisible, AuxMethods);
//            elementVisible.clear();
//            elementVisible.sendKeys(value);
//        } else {
//            Assert.fail("The input that you are searching is not visible");
//        }
//    }



    public void clickElement(WebElement elementToClick) {
        WebElement element = Waits.waitForElementToBeClickable(wait, elementToClick);
        if (element != null) {

            elementToClick.click();

        } else {
            System.out.println("Error on click, element is not visible. " + elementToClick.toString());
            Assert.fail("Error on click, element is not visible. " + elementToClick.toString());

        }
    }

    public void fillInput(WebElement elementToVerify, String value)  throws InterruptedException {

        WebElement elementVisible = Waits.waitVisibilityOfElement(wait, elementToVerify);
        if (elementVisible != null) {

            elementVisible.clear();
            elementVisible.sendKeys(value);
        } else {
            Assert.fail("The input that you are searching is not visible");
        }
    }

    public void clearSessionStorageJS() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.sessionStorage.clear();");
    }

    public void clearLocalStorageJS() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.localStorage.clear();");
    }



}
