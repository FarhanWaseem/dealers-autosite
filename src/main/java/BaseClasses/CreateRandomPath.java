package BaseClasses;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CreateRandomPath {

    public static String generateTestName(){
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
        String TestName="FS"+timeStamp;
        return TestName;

    }

}
