package LeadSubmission;

import Utilities.CommonActions;
import Utilities.GlobalVariables;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LeadActions extends LeadElements {

    private WebDriver driver;
    private WebDriverWait wait;
    private CommonActions commonActions;

    public LeadActions(WebDriver driver, WebDriverWait wait) {
        super();
        this.driver = driver;
        this.wait = wait;
        commonActions = new CommonActions(driver, wait);
        PageFactory.initElements(driver, this);
    }


    // -- Home Page Elements -- //

    private void clickmake() {
        commonActions.clickElement(Make);
    }

    private void clickmakeselect() {
        commonActions.clickElement(MakeSelect);
    }

    private void clickmodel() {
        commonActions.clickElement(Model);
    }

    private void clickmodelselect() {
        commonActions.clickElement(ModelSelect);
    }

    private void clicktrim() {
        commonActions.clickElement(Trim);
    }

    private void clicktrimselect() {
        commonActions.clickElement(TrimSelect);
    }

    private void fillzipcode(String Zip) throws InterruptedException {
        commonActions.fillInput(Zipcode, Zip);
    }

    private void clickgetstarted() {
        commonActions.clickElement(GetStartedButton);
    }

    // -- Quote Page Elements -- //

    private void clickdealers() {
        commonActions.clickElement(Dealers);
    }

    private void fillfirstname(String fname) throws InterruptedException {
        commonActions.fillInput(Firstname, fname);
    }

    private void filllastname(String lname) throws InterruptedException {
        commonActions.fillInput(Lastname, lname);
    }

    private void filladdress(String address) throws InterruptedException {
        commonActions.fillInput(Address, address);
    }

    private void fillphone1(String phone1) throws InterruptedException {
        commonActions.fillInput(Phone1, phone1);
    }

    private void fillphone2(String phone2) throws InterruptedException {
        commonActions.fillInput(Phone2, phone2);
    }

    private void fillphone3(String phone3) throws InterruptedException {
        commonActions.fillInput(Phone3, phone3);
    }

    private void fillemail(String email) throws InterruptedException {
        commonActions.fillInput(Email, email);
    }

    private void clickgetprice() {
        commonActions.clickElement(GetStartedQuote);
    }

    // -- Thank you Page Elemenets --//

    private void clickiconsent(){commonActions.clickElement(Iconsent);}

    private void clickmaketyp(){commonActions.clickElement(MakeTYP);}
    private void clickmakeselettyp(){commonActions.clickElement(MakeSelectTYP);}

    private void clickmodeltyp(){commonActions.clickElement(ModelTYP);}
    private void clickmodelselecttyp(){commonActions.clickElement(ModelSelectTYP);}

    private void clickgetpricetyp(){commonActions.clickElement(GoTYP);}


    public void LeadSubmission(String Zipcode, String Firstname, String Lastname, String Address, String Phone1,
                               String Phone2,
                               String Phone3, String Email) throws InterruptedException {
        commonActions.navigateToUrl(GlobalVariables.host);
        clickmake();
        clickmakeselect();
        clickmodel();
        clickmodelselect();
        clicktrim();
        clicktrimselect();
        fillzipcode(Zipcode);
        clickgetstarted();
        Thread.sleep(3000);
        driver.close();
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);}

        Thread.sleep(3000);clickdealers();
            fillfirstname(Firstname);
            filllastname(Lastname);
            filladdress(Address);
            fillphone1(Phone1);
            fillphone2(Phone2);
            fillphone3(Phone3);
            fillemail(Email);
            clickgetprice();


        }

    public void SecondaryLead()throws InterruptedException{

        clickiconsent();
        clickmaketyp();clickmakeselettyp();
        clickmodeltyp();clickmodelselecttyp();
        clickgetpricetyp();Thread.sleep(1000);
        clickdealers();Thread.sleep(1000);
        clickgetprice();
    }

}

