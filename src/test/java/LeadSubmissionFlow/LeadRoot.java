package LeadSubmissionFlow;

import AdminPanelElements.AdminActions;
import BaseClasses.DriverBaseClass;
import LeadSubmission.LeadActions;
import Utilities.GlobalVariables;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.io.IOException;


public class LeadRoot extends DriverBaseClass {

    LeadActions Leadactions;
    ExtentTest test;


    @BeforeClass(alwaysRun = true)
    public void initPageObectClass() {Leadactions = new LeadActions(driver, wait);}

    @Test(priority = 3, description = "Verifying Lead submission from Root Path Functionality", groups = "Lead Submission")
    public void LeadSubmissionRoot() throws InterruptedException

    {


        System.out.println(GlobalVariables.env);
        Leadactions.LeadSubmission(GlobalVariables.Zipcode, GlobalVariables.FirstName,
                GlobalVariables.LastName, GlobalVariables.Address,
                GlobalVariables.Phone1, GlobalVariables.Phone2,
                GlobalVariables.Phone3, GlobalVariables.Email);

        Thread.sleep(5000);}

        @Test(priority = 4, description = "Verifying Lead submission from Root Path Functionality", groups = "Lead Submission")
                public void SecondaryLeadRoot() throws InterruptedException{


        Leadactions.SecondaryLead();
    }

    }


