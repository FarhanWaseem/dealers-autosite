package AdminPanel;


import AdminPanelElements.AdminActions;
import BaseClasses.DriverBaseClass;
import Utilities.GlobalVariables;
import javafx.scene.layout.Priority;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AdminLogin extends DriverBaseClass {

    AdminActions Adminaction;

    @BeforeClass(alwaysRun = true)
    public void initPageObectClass() {Adminaction = new AdminActions(driver, wait);}

    @Test(priority =1 , description = "Verifying Login Functionality", groups = "Admin Panel")

    public void Login() throws InterruptedException{

        System.out.println(GlobalVariables.env);

        //Adminaction.fb();

        Adminaction.AdminLogin(GlobalVariables.UsernameAdmin, GlobalVariables.PasswordAdmin);

    }


}
