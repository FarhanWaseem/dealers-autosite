package AdminPanel;

import AdminPanelElements.AdminActions;
import BaseClasses.DriverBaseClass;
import Utilities.GlobalVariables;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CreatePath extends DriverBaseClass {

    AdminActions Adminaction;

    @BeforeClass (alwaysRun = true)

    public void intiPageObjectClass(){Adminaction = new AdminActions(driver, wait);}


    @Test(priority = 2, description = "Verifying Create Path Functionality", groups = "Admin Panel")

    public void CreatePath() throws InterruptedException{

       Adminaction.AdminLogin(GlobalVariables.UsernameAdmin, GlobalVariables.PasswordAdmin);
       Adminaction.CreatePath(GlobalVariables.PathNameAdmin, GlobalVariables.PathLabelAmin);
    }

}
