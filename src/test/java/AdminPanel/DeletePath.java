package AdminPanel;

import AdminPanelElements.AdminActions;
import BaseClasses.DriverBaseClass;
import Utilities.GlobalVariables;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DeletePath extends DriverBaseClass {

    AdminActions Adminaction;

    @BeforeClass(alwaysRun = true)

    public void initPageObjectClass(){ Adminaction = new AdminActions(driver, wait);}

    @Test(priority = 5, description = "Verifying Delete Path Functionality", groups = "Admin Panel")

    public void DeletePath() throws InterruptedException{
        System.out.println(GlobalVariables.env);

        Adminaction.AdminLogin(GlobalVariables.UsernameAdmin, GlobalVariables.PasswordAdmin);
        Adminaction.DeletePath();
    }

}
